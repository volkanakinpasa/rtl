﻿
using System;
using System.Collections.Generic;

namespace Model
{
    public class Cast
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
    }
}