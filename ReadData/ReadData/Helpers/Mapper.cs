﻿using Model;
using System.Collections.Generic;
using System.Linq;

namespace ReadData.Helpers
{
    public static class Mapper
    {
        /// <summary>
        /// Could be replaced with AutoMapper...
        /// </summary>
        /// <param name="entityTvShows"></param>
        /// <returns></returns>
        public static List<TvShow> Map(this List<DataLayer.Entities.TvShow> entityTvShows)
        {
            List<TvShow> tvShows = new List<TvShow>();

            entityTvShows.ForEach((DataLayer.Entities.TvShow entityTvShow) =>
            {
                List<Cast> casts = new List<Cast>();
                foreach (var tvShowCast in entityTvShow.TvShowCasts)
                {
                    casts.Add(new Cast()
                    {
                        Id = tvShowCast.Cast.Id,
                        Name = tvShowCast.Cast.Name,
                        Birthday = tvShowCast.Cast.Birthday,
                    });
                }

                casts = casts.OrderByDescending(c => c.Birthday).ToList();

                var tvShow = new TvShow()
                {
                    Id = entityTvShow.Id,
                    Name = entityTvShow.Name,
                    Cast = casts
                };


                tvShows.Add(tvShow);
            });

            return tvShows;
        }
    }
}
