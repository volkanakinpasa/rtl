﻿using System.Collections.Generic;
using DataLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Model;
using ReadData.Helpers;

namespace ReadData.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReadController : ControllerBase
    {
        public IReadContext ReadContext { get; }
        public IConfiguration Configuration { get; }

        public ReadController(IReadContext readContext, IConfiguration configuration)
        {
            ReadContext = readContext;
            Configuration = configuration;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TvShow>> Get(int page)
        {
            List<DataLayer.Entities.TvShow> entities = ReadContext.Read(page, Configuration.GetValue<int>("pageSize"));

            return entities.Map();
        }
    }
}
