﻿using DataLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
    public class ReadContext : DbContext, IReadContext
    {
        public DbSet<DataLayer.Entities.TvShow> TvShows { get; set; }
        public DbSet<DataLayer.Entities.Cast> Casts { get; set; }
        public DbSet<DataLayer.Entities.TvShowCast> TvShowCasts { get; set; }

       
        public ReadContext(DbContextOptions<ReadContext> options)
            : base(options)
        {

        }

        public List<DataLayer.Entities.TvShow> Read(int page, int pageSize)
        {
            return TvShows.Skip(page * pageSize).Take(pageSize)
                 .Include(ts => ts.TvShowCasts)
                 .ThenInclude((TvShowCast tsc) => tsc.Cast)
                 .ToList();
        }
    }
}
