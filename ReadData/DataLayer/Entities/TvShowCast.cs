﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataLayer.Entities
{
    public class TvShowCast
    {
        public int Id { get; set; }
        public int TvShowId { get; set; }
        public int CastId { get; set; }

        [ForeignKey("TvShowId")]
        public DataLayer.Entities.TvShow TvShow { get; set; }

        [ForeignKey("CastId")]
        public DataLayer.Entities.Cast Cast { get; set; }
    }
}