﻿
using System;
using System.Collections.Generic;

namespace DataLayer.Entities
{
    public class Cast
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public ICollection<TvShowCast> TvShowCasts { get; set; }
    }
}