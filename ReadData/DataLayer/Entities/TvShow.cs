﻿using System.Collections.Generic;

namespace DataLayer.Entities
{
    public class TvShow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<TvShowCast> TvShowCasts { get; set; }
    }
}