﻿using System.Collections.Generic;

namespace DataLayer
{
    public interface IReadContext
    {
        List<DataLayer.Entities.TvShow> Read(int page, int pageSize);
    }
}