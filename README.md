
# RTL - Developer Candidate Assignment #


I created two projects. The first project(FetchData) is a console application, scrapes the TVMaze API for show and cast information and then persists the data in MsSql. The second project(ReadData) is a web api, reads paginated data from database. The list of the cast is ordered by birthday descending. 


### Build & Run ###
 - Run ./sql/Init.sql to create a database and tables
 - FetchData: Build & Run ./FetchData/FetchData.sln solution
 - ReadData: Build & Run ./ReadData/ReadData.sln solution

### Information ###
 FetchData requests http://api.tvmaze.com/shows?page=[num] and keeps the number in database to be able fetch next chunk at next run. When there is no list from api, the app receives a HTTP 404 response code and stops itself.

ReadData: Provides an endpoint /api/Read?page=[num], a paginated list of all tv shows and casts(ordered by birthday descending).

 