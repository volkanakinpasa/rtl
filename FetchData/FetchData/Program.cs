﻿using Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using System.IO;
using Model;
using System;
using System.Collections.Generic;

namespace FetchData
{
    class Program
    {
        static ServiceProvider provider;
        static Config _config;

        static void Main(string[] args)
        {
            Configure();

            Start();
        }

        private static void Configure()
        {
            var builder = new ConfigurationBuilder()
                          .SetBasePath(Directory.GetCurrentDirectory())
                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration configuration = builder.Build();

            _config = new Config();

            configuration.GetSection("Config").Bind(_config);

            provider = new ServiceCollection()
             .AddServiceLayer(_config.ConnectionString)
             .AddSingleton<IFetchDataService, FetchDataService>()
             .BuildServiceProvider();
        }

        static void Start()
        {
            var fetchService = provider.GetService<IFetchDataService>();

            try
            {
                fetchService.Import(_config.TimesOfCall);
            }
            catch (KeyNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
