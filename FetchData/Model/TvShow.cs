﻿using System.Collections.Generic;

namespace Model
{
    public class TvShow
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Cast> Casts { get; set; }
    }
}