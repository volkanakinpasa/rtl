﻿namespace Model
{
    public class Config
    {
        public int TimesOfCall { get; set; }
        public string ConnectionString { get; set; }
    }
}
