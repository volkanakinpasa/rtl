﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Model;

namespace DataLayer
{
    public class DataRepository : IDataRepository
    {
        private readonly string _connectionString;

        public DataRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public void ImportShows(List<TvShow> tvShows, int lastPageNumber)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {
                    connection.Open();

                    using (SqlTransaction trans = connection.BeginTransaction())
                    {
                        foreach (var tvShow in tvShows)
                        {
                            using (SqlCommand command = new SqlCommand("", connection, trans))
                            {
                                command.CommandText = "If Not Exists (Select Id from TvShows Where Id = @Id) Begin insert into TvShows ([Id], [Name]) VALUES(@Id, @Name) End";
                                command.Parameters.Add(new SqlParameter("@Id", tvShow.Id));
                                command.Parameters.Add(new SqlParameter("@Name", tvShow.Name));
                                command.ExecuteNonQuery();
                            }

                            using (SqlCommand command = new SqlCommand("", connection, trans))
                            {
                                int number = 0;
                                foreach (var cast in tvShow.Casts)
                                {
                                    if (cast.Person == null)
                                    {
                                        continue;
                                    }
                                    number++;
                                    command.CommandText = string.Format("If Not Exists (Select Id from Casts Where Id = @CastId{0}) Begin insert into Casts ([Id], [Name], [Birthday]) VALUES(@CastId{0}, @CastName{0}, @CastBirthday{0}) End", number);
                                    command.Parameters.Add(new SqlParameter(string.Format("@CastId{0}", number), cast.Person.Id));
                                    command.Parameters.Add(new SqlParameter(string.Format("@CastName{0}", number), cast.Person.Name));
                                    if (cast.Person.Birthday != null)
                                    {
                                        command.Parameters.Add(new SqlParameter(string.Format("@CastBirthday{0}", number), cast.Person.Birthday));
                                    }
                                    else
                                    {
                                        command.Parameters.Add(new SqlParameter(string.Format("@CastBirthday{0}", number), DBNull.Value));
                                    }

                                    command.ExecuteNonQuery();
                                }
                            }

                            using (SqlCommand command = new SqlCommand("", connection, trans))
                            {
                                int number = 0;
                                foreach (var cast in tvShow.Casts)
                                {
                                    if (cast.Person == null)
                                    {
                                        continue;
                                    }

                                    number++;
                                    command.CommandText = string.Format("If Not Exists (Select Top 1 TvShowId from TvShowCasts where TvShowId =@TvShowId{0} AND CastId = @CastId{0}) BEGIN insert into TvShowCasts ([TvShowId], [CastId]) VALUES(@TvShowId{0}, @CastId{0}) End", number);
                                    command.Parameters.Add(new SqlParameter(string.Format("@TvShowId{0}", number), tvShow.Id));
                                    command.Parameters.Add(new SqlParameter(string.Format("@CastId{0}", number), cast.Person.Id));
                                    command.ExecuteNonQuery();
                                }
                            }

                            using (SqlCommand command = new SqlCommand("", connection, trans))
                            {
                                command.CommandText = "Update LastPage SET PageNumber = @LastPageNumber";
                                command.Parameters.Add(new SqlParameter("@LastPageNumber", lastPageNumber));
                                command.ExecuteNonQuery();
                            }
                        }

                        trans.Commit();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public int ReadLastPageNumber()
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand command = new SqlCommand("select PageNumber from LastPage", connection))
                {
                    return (int)command.ExecuteScalar();
                }
            }

        }
    }
}
