﻿using Model;
using System.Collections.Generic;

namespace DataLayer
{
    public interface IDataRepository
    {
        int ReadLastPageNumber();
        void ImportShows(List<TvShow> tvShows, int lastPageNumber);
    }
}