﻿using DataLayer;
using Model;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Service
{
    public class FetchDataService : IFetchDataService
    {
        private const string apiUrl = "http://api.tvmaze.com";
        public IDataRepository DataRepository { get; }
        public FetchDataService(IDataRepository dataRepository)
        {
            DataRepository = dataRepository;
        }

        public void Import(int timesOfCall)
        {
            int lastPageNumber = DataRepository.ReadLastPageNumber();

            for (int i = 0; i < timesOfCall; i++)
            {
                List<TvShow> tvShows = new List<TvShow>();

                FetchTvShows(ref tvShows, lastPageNumber);

                FetchCasts(tvShows);

                DataRepository.ImportShows(tvShows, lastPageNumber);

                ++lastPageNumber;
            }
        }

        private void FetchCasts(List<TvShow> tvShows)
        {
            var allTasks = tvShows.Select(async tvShow =>
            {
                tvShow.Casts = await FetchCasts(tvShow.Id);
            });

            Task.WhenAll(allTasks);
        }

        private void FetchTvShows(ref List<TvShow> tvShows, int lastPageNumber)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("shows", Method.GET);
            request.AddParameter("page", lastPageNumber);

            IRestResponse<List<TvShow>> response = client.Execute<List<TvShow>>(request);

            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                throw new KeyNotFoundException("There is no more Tv shows to fetch");
            }

            tvShows = response.Data;
        }

        private async Task<List<Cast>> FetchCasts(int tvShowId)
        {
            var client = new RestClient(apiUrl);

            var request = new RestRequest("shows/{id}/cast", Method.GET);

            request.AddUrlSegment("id", tvShowId);

            IRestResponse<List<Cast>> response = await client.ExecuteTaskAsync<List<Cast>>(request);

            return response.Data;
        }
    }
}