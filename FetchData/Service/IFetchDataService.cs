﻿using Model;
using System.Collections.Generic;

namespace Service
{
    public interface IFetchDataService
    {
        void Import(int timesOfCall);
    }
}