﻿using DataLayer;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service
{

    public static class ServiceCollectionExtension
    {
        /// <summary>
        /// Add other layer dependencies
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServiceLayer(this IServiceCollection collection, string connectionString)
        {
            collection.AddSingleton<IDataRepository, DataRepository>(factory => {
                return new DataRepository(connectionString);
            });

            return collection;
        }
    }

}
